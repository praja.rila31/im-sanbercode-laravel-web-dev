<?php
require('animal.php'); 
require('Ape.php');
require('Frog.php');

$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "legs : " . $sheep->legs . "<br>"; // 4
echo "cool blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

$kodok = new Frog("buduk");

echo "Name : " . $kodok->name . "<br>"; 
echo "legs : " . $kodok->legs . "<br>"; 
echo "cold blooded : " . $kodok->cold_blooded . "<br>"; 
$kodok->jump();

echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");

echo "Name : " . $sungokong->name . "<br>"; 
echo "legs : " . $sungokong->legs . "<br>"; 
echo "cold blooded : " . $sungokong->cold_blooded . "<br>"; 
$sungokong->yell() . "<br>";


?>