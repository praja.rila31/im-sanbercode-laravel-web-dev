@extends('layouts.master')
@section('title')
Halaman Tambah Cast   
@endsection
@section('sub-title')
    Cast
@endsection
@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @method('put')
    @csrf 
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" value="{{$cast->nama}}" name="nama">
        </div>
        <div class="form-group">
            <label >Umur</label>
            <input type="number" class="form-control" value="{{$cast->umur}}" name="umur">
        </div>
        <div class="form-group">
            <label >Bio</label>
            <textarea  class="form-control" name="bio">{{$cast->bio}}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection

    
