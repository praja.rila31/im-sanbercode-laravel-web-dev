@extends('layouts.master')
@section('title')
Halaman Tambah Cast   
@endsection
@section('sub-title')
    Cast
@endsection
@section('content')
<form action="/cast" method="POST">
    @csrf 
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name="nama">
        </div>
        <div class="form-group">
            <label >Umur</label>
            <input type="number" class="form-control" name="umur">
        </div>
        <div class="form-group">
            <label >Bio</label>
            <textarea  name="bio" class="form-control" ></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection

    
