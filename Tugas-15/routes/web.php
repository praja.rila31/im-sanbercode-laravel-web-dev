<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', [AuthController::class, 'welcome']);
Route::get('/', [HomeController::class, 'Home']);
Route::get('/Register', [AuthController::class, 'Register']);

Route::post('/kirim', [AuthController::class, 'send']);

Route::get('/data-table', function(){
    return view('halaman.datatable');
});

Route::get('/table', function(){
    return view('halaman.table');
});

//CRUD cast 
Route::get('/cast/create', [CastController::class, 'create']);

Route::post('/cast', [CastController::class, 'store']);

Route::get('/cast', [CastController::class, 'Home']);

Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//Update Cast
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

//Route untuk update Data Cast
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//Delete Cast
//Route untuk delete cast berdasrkan id 
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
