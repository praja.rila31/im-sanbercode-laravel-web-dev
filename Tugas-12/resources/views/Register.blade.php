<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Halaman Register</title>
</head>
<body>
    <h2>Buat Account Baru!</h2>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="POST">
        @csrf 
        <label>First name: </label><br>
        <input type="text" name="fname"><br><br>
        <label>Last name: </label><br>
        <input type="text" name="lname"><br><br>
        <label>Gender:</label><br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br><br>
        <label>Nationality:</label><br><br>
        <select name="national">
            <option value="">Indonesia</option>
            <option value="">Inggris</option>
            <option value="">Malaysia</option>
            <option value="">Singapure</option>
        </select><br><br>
        <label >Language Spoken:</label><br>
        <input type="checkbox" name="Language"> Bahasa Indonesia<br>
        <input type="checkbox" name="Language"> English<br>
        <input type="checkbox" name="Language"> Other<br><br>
        <label>Bio:</label><br>
        <textarea cols="25" rows="10"></textarea><br>
        <input type="submit" value="kirim">
        {{-- <a href="/welcome"></a> --}}
    </form>
</body>
</html>